""""""""""""""""""""""""""" 
"" Plugged 
":"""""""""""""""""""""""""
call plug#begin()
" A fuzzy file finder
Plug 'kien/ctrlp.vim'
" Comment/Uncomment tool
Plug 'scrooloose/nerdcommenter'
" Switch to the begining and the end of a block by pressing %
Plug 'tmhedberg/matchit'
" A Tree-like side bar for better navigation
Plug 'scrooloose/nerdtree'
" Git integration
Plug 'tpope/vim-fugitive'
" Auto-close braces and scopes
Plug 'jiangmiao/auto-pairs'
" Intellisense
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Rust
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

" (Optional) Multi-entry selection UI.
Plug 'junegunn/fzf'
" Markdown Preview (requires yarn)
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install' }
" Markdown syntax
Plug 'vim-pandoc/vim-pandoc-syntax'
" Zen mode
Plug 'folke/zen-mode.nvim'
" Markdown table of contents
Plug 'ajorgensen/vim-markdown-toc'
" Treesitter syntax higlighting
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
" Sonokai theme
Plug 'sainnhe/sonokai'
" SuperCollider
Plug 'davidgranstrom/scnvim'
" Completion engine
Plug 'hrsh7th/nvim-cmp'
" Completion source for tags
Plug 'quangnguyen30192/cmp-nvim-tags'
" Latex
Plug 'lervag/vimtex'
call plug#end()

"""""""""""""""""""""""""""
"" Mappings
"""""""""""""""""""""""""""
" Code action on <leader>a
vmap <leader>a <Plug>(coc-codeaction-selected)<CR>
nmap <leader>a <Plug>(coc-codeaction-selected)<CR>

" Format action on <leader>f
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
" Goto definition
nmap <silent> gd <Plug>(coc-definition)
" Open definition in a split window
nmap <silent> gv :vsp<CR><Plug>(coc-definition)<C-W>L

" Tab autocomplete
inoremap <silent><expr> <tab> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<TAB>"
inoremap <silent><expr> <cr> "\<c-g>u\<CR>"

"""""""""""""""""""""""""""
"" Settings 
"""""""""""""""""""""""""""
set nu
set clipboard=unnamedplus
set splitright
set tabstop=4       " The width of a TAB is set to 4.
set shiftwidth=4    " Indents will have a width of 4
set softtabstop=4   " Sets the number of columns for a TAB
set expandtab       " Expand TABs to spaces
" Indents word-wrapped lines as much as the 'parent' line
set breakindent
" Ensures word-wrap does not split words
set formatoptions=l
set lbr

" Color scheme
if has('termguicolors')
set termguicolors
endif
let g:sonokai_style = 'default'
let g:sonokai_better_performance = 1
colorscheme sonokai
"""""""""""""""""""""""""""

"" Lua 
"""""""""""""""""""""""""""

lua << EOF
-- SuperCollider --
local scnvim = require 'scnvim'
local map = scnvim.map
local map_expr = scnvim.map_expr
scnvim.setup {
  keymaps = {
    ['<M-e>'] = map('editor.send_line', {'i', 'n'}),
    ['<C-e>'] = {
      map('editor.send_block', {'i', 'n'}),
      map('editor.send_selection', 'x'),
    },
    ['<CR>'] = map('postwin.toggle'),
    ['<M-CR>'] = map('postwin.toggle', 'i'),
    ['<M-L>'] = map('postwin.clear', {'n', 'i'}),
    ['<C-k>'] = map('signature.show', {'n', 'i'}),
    ['<F12>'] = map('sclang.hard_stop', {'n', 'x', 'i'}),
    ['<leader>st'] = map('sclang.start'),
    ['<leader>sk'] = map('sclang.recompile'),
    ['<F1>'] = map_expr('s.boot'),
    ['<F2>'] = map_expr('s.meter'),
  },
  editor = {
    highlight = {
      color = 'IncSearch',
    },
  },
    postwin = {
    highlight = true,
    auto_toggle_error = true,
    scrollback = 5000,
    horizontal = false,
    direction = 'right',
    size = 64,
    fixed_size = nil,
    keymaps = nil,
    float = {
      enabled = false,
      row = 0,
      col = function()
        return vim.o.columns
      end,
      width = 64,
      height = 14,
      config = {
        border = 'single',
      },
        callback = function(id)
        vim.api.nvim_win_set_option(id, 'winblend', 10)
      end,
    }
  },
}
EOF

lua << EOF
-- SuperCollider Completion --
local cmp = require'cmp'

cmp.setup({
snippet = {
	expand = function(args)
	end,
	},
sources = {
	{ name = 'tags' },
}
})
EOF

lua << EOF
-- Wrap post window --
require('scnvim.postwin').on_open:append(function()
  vim.opt_local.wrap = true
end)
EOF
lua << EOF
-- Treesitter --
require'nvim-treesitter.configs'.setup {
   ensure_installed = {
     "c", "vim", "lua", "query", "css", "html", "json", "javascript", "typescript", "tsx", "markdown", "python", "supercollider", "rust", "cmake"
},
  highlight = {
    enable = true,
    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
  indent = {
    enable = {},
    disable = {"supercollider", "html"}
  },
  autotag = {
    enable = true
  }

}
local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
parser_config.tsx.filetype_to_parsername = { "javascript", "typescript.tsx" }
EOF
